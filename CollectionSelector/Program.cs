﻿using System;
using System.Collections.Generic;

namespace CollectionSelector
{
    class Program
    {
        static void Main(string[] args)
        {
            //Task Create a new console application named CollectionSelector.
            //Create an empty Collection of each type covered above. You will extend this at the end of this lesson

            //Task: Extend the CollectionSelector application created in the previous task.
            //Allow a user to store 5 strings in any one of the Generic collections of their choice.
            //Then display the contents of the collection to the console.

            List<string> list = new List<string>();
            Queue<string> queue = new Queue<string>();
            Stack<string> stack = new Stack<string>();
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            SortedList<string, string> sortedList = new SortedList<string, string>();
            int collection;
            string input;


            Console.WriteLine("Choose an input type (1 for List, 2 for Queue, 3 for Stack, 4 for Dictionary, 5 for SortedList )");
            collection = int.Parse(Console.ReadLine());

            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine("Enter a value");
                input = Console.ReadLine();

                switch (collection)
                {
                    case 1:
                        //List
                        list.Add(input);
                        break;

                    case 2:
                        //queue
                        queue.Enqueue(input);
                        break;
                    case 3:
                        //stack
                        stack.Push(input);
                        break;
                    case 4:
                        // Dic
                        dictionary.Add($"Key-{input}", input);
                        break;
                    case 5:
                        //SortedList
                        sortedList.Add($"Key-{input}", input);
                        break;
                    default:

                        break;
                }
            }

           

        }

        
    }
}
